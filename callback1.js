module.exports = function getBoard(boardsData, boardId, cb) {
  try {
    if (arguments.length !== 3) throw new Error("Some parameter is missing");
    if (!Array.isArray(boardsData))
      throw new Error("Board data is not an array");

    setTimeout(() => {
      let data = boardsData.find((element) => element.id === boardId);
      if (data) {
      cb(null, data);
      } else {
        cb("id not found", null);
      }
    }, 2 * 1000);
  } catch (err) {
    console.error(err.message);
  }
};

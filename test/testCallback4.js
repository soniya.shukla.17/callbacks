let boardsData = require("./../boards.json")
let cardsData = require("./../cards.json")
let listData = require("./../lists.json")

thanosInformation = require("./../callback4")

getBoard = require("./../callback1")

getList = require("./../callback2")

getCard = require("./../callback3")

let allInformation={
    "boardsData" : boardsData,
    "listData" : listData,
    "cardsData" : cardsData,
    "getBoard" : getBoard,
    "getList": getList,
    "getCard" : getCard,
    "boardName": "Thanos"

}


thanosInformation(allInformation,(err,data)=>{
    if(err)
    {
        console.error("err.message")
    }
    else{
        console.log(data)
    }
})

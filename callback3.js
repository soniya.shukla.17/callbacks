module.exports = function getCards(cardsData,listId, cb) {
  try {
    if (arguments.length !== 3) throw new Error("Some parameter is missing");

    if(typeof cardsData !== 'object')
      throw new Error("card data is not a object")
    setTimeout(() => {
      let data = cardsData.hasOwnProperty(listId)
      if (data) {    
        cb(null, cardsData[listId]);
      } else {
        cb("id not found", null);
      }
    }, 2 * 1000);
  } catch (err) {
    console.error(err.message);
  }
};

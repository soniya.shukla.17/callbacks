module.exports = function getList(listData, boardId, cb) {
    try {
      if (arguments.length !== 3) throw new Error("Some parameter is missing");
      
      if(typeof listData !== 'object')
        throw new Error("list data is not a object")
      setTimeout(() => {
        
        if (listData.hasOwnProperty(boardId)) {
            
        cb(null, listData[boardId]);
        } else {
          cb("id not found", null);
        }
      }, 2 * 1000);
    } catch (err) {
      console.error(err.message);
    }
  };
  
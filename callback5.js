module.exports = function thanosInformation(allInformation, cb) {
  let thanosObject = allInformation.boardsData.find(
    (element) => element.name === allInformation.boardName
  );
  //console.log(thanosObject);
  try {
    if (arguments.length !== 2) throw new Error("Some parameter is missing");
    if (!Array.isArray(allInformation.boardsData))
      throw new Error("Board data is not an array");
    if (typeof allInformation.listData !== "object")
      throw new Error("list data is not a object");
    setTimeout(() => {
      allInformation.getBoard(
        allInformation.boardsData,
        thanosObject.id,
        (err, data) => {
          if (err) {
            console.error(err.message);
          } else {
            console.log(data);
            allInformation.getList(
              allInformation.listData,
              data.id,
              (err, data) => {
                if (err) {
                  console.error(err.message);
                } else {
                  let offset = 0;
                  console.log(data);
                  let mindId = data.filter(
                    (element) =>
                      element.name === "Mind" || element.name === "Space"
                  );
                  mindId.forEach((obj) => {
                    allInformation.getCard(
                      allInformation.cardsData,
                      obj.id,
                      (err, data) => {
                        if (err) {
                          console.error(err.message);
                        } else {
                          data.forEach((element) => {
                            setTimeout(() => {
                              console.log(element);
                            }, 2000 + offset);
                            offset += 1000;
                          });
                        }
                      }
                    );
                  });
                }
              }
            );
          }
        }
      );
    }, 2 * 1000);
  } catch (err) {
    console.error(err.message);
  }
};
